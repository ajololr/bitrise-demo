module.exports = {
  testRunner: 'jest',
  runnerConfig: 'e2e/config.json',
  configurations: {
    'ios.sim': {
      binaryPath:
        'ios/build/Build/Products/Release-iphonesimulator/bitriseDemo.app',
      build:
        'xcodebuild -workspace ios/bitriseDemo.xcworkspace -scheme "bitriseDemo" -configuration "Release" -sdk iphonesimulator -derivedDataPath ios/build',
      type: 'ios.simulator',
      device: {
        type: 'iPhone 12',
      },
    },
    'android.emu': {
      binaryPath: 'android/app/build/outputs/apk/release/app-release.apk',
      testBinaryPath:
        'android/app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk',
      build:
        './android/gradlew -p android assembleRelease assembleAndroidTest -DtestBuildType=release',
      type: 'android.emulator',
      device: {
        avdName: 'Pixel_API_28_AOSP',
      },
    },
  },
};
