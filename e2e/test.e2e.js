import {expect} from 'detox';

describe('Example (hello)', () => {
  beforeEach(async () => {
    await device.launchApp();
  });

  it('should have welcome screen', async () => {
    await expect(
      element(by.text('Read the docs to discover what to do next:')),
    ).toExist();
  });
});
