module.exports = {
  projects: [
    {
      testMatch: ['**/?(*.)+(spec|test)\\.[jt]s?(x)'],
      displayName: {
        name: 'Common',
        color: 'cyan',
      },
    },
  ],
};
